# Petra's Framework

*Use this framework to kick-start your new project. 🚀*

*It uses the following NPM dependencies:*

* dotenv
* ejs
* express
* express-session
* mongodb
* session-file-store

*and NPM dev-dependencies:*

* node-sass
* nodemon

## Installation

1. Clone the project (The Bitbucket repository is [HERE](https://bitbucket.org/petra-jakobcic/framework)).   
   ```
   git clone https://bitbucket.org/petra-jakobcic/framework
   ```
1. Install the NPM dependencies.   
   ```
   npm install
   ```
1. Configure your app & database settings in the `.env` file.   
   ```
   cp .env.example .env
   ```
   Set the variables to your own project's needs.
1. Launch the development server with *nodemon* to reload the server every time you save your work.   
   ```
   npm run dev
   ```
1. Delete the .git file and initialise your own Git repository.   
   ```
   git init
   ```

## Customise

### CSS
You can create your own CSS files in the `public` folder.
Alternatively, there are Sass files and you can run the NPM script `npm run compile-css` to generate the default CSS file.

### Controllers
To create your own controller:
- copy the content of the example `pageController.js` file into a new file in the 'controllers' folder (e.g. named `myNewController.js`) and make sure to include it in the `configureRoutes.js` file.