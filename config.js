// The 'dotenv' module creates variables based on the .env file and attaches them to process.env, which is empty by default.
require("dotenv").config();

const config = {
    port: process.env.APP_PORT,
    sessionSecret: process.env.SESSION_SECRET,
    mongoHost: process.env.MONGO_HOST,
    mongoPort: process.env.MONGO_PORT,
    mongoUserName: process.env.MONGO_USERNAME,
    mongoPassword: process.env.MONGO_PASSWORD,
    mongoDbName: process.env.MONGO_DB_NAME
};

module.exports = config;
