const mongodb = require('mongodb');
const mongo = mongodb.MongoClient;
const config = require('./config');

let userNameAndPassword = "";

if (config.mongoUserName && config.mongoPassword) {
    userNameAndPassword = config.mongoUserName + ":" + config.mongoPassword + "@";
}

const mongoUrl = `mongodb://${userNameAndPassword}${config.mongoHost}:${config.mongoPort}/${config.mongoDbName}`;

let db = null;

// Connect to MongoDB.
const getDB = callback => {
    if (db) {
        callback(db);
    } else {
        mongo.connect(
            mongoUrl,
            { useUnifiedTopology: true },
            (err, connection) => {
                if (err) {
                    throw "Error connecting to the database...";
                }
            
                db = connection.db(config.mongoDbName);

                callback(db);
            }
        );
    }
}

module.exports = getDB;
