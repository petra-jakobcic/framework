# Project Name

*Write a short project summary here...*

## Installation

1. Clone the project (The Bitbucket repository is [HERE](https://<linkToTheRepository>)).   
   ```
   git clone https://<linkToTheRepository>
   ```
1. Install NPM dependencies.   
   ```
   npm install
   ```
1. Start the server.   
   ```
   npm start
   ```
