const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
    res.render("layout", {
        pageTitle: "Welcome",
        content: "index"
    });
});

router.get("/about", (req, res) => {
    res.render("layout", {
        pageTitle: "About",
        content: "about"
    });
});

module.exports = router;
