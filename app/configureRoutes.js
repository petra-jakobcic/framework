// This is an example controller.
// Add your own controllers in the same way.
const pageController = require("./controllers/pageController");

function configureRoutes(app) {
    // Make sure you add your controllers to the app here!
    app.use('/', pageController);
}

module.exports = configureRoutes;
