// Require your dependencies.
const express = require("express");
const session = require("express-session");
const fileStore = require("session-file-store")(session);
const path = require("path");
const config = require("./config");
const configureRoutes = require("./app/configureRoutes");

// Create a server.
const app = express();

// Use the session middleware.
const optionsForFileStore = {};

app.use(session({
    name: "server-session-cookie-id",
    secret: config.sessionSecret,
    resave: false,
    saveUninitialized: true,
    store: new fileStore(optionsForFileStore),
    cookie: { maxAge: 60 * 60 * 24 * 1000 }
}));

// Make a static folder.
app.use(express.static(path.join(__dirname, "public")));

// Turn the payload of a POST request from text to an object of key/value pairs.
app.use(express.urlencoded({extended: true}));

// Prepare the server with its routes.
configureRoutes(app);

// Tell Express to use EJS.
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "app/views"));

// Make the server listen on a port.
app.listen(config.port, () => {
    console.log(`About to listen on port ${config.port}`);
    console.log(`Now listening on port ${config.port}`);
});
